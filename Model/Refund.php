<?php

/*
 * Refund.php
 */

namespace AzureSpring\Wxpay\Model;

/**
 * Refund
 */
class Refund
{
    const STATUS_OK       = 'SUCCESS';
    const STATUS_REJECTED = 'REFUNDCLOSE';
    const STATUS_WAIT     = 'PROCESSING';
    const STATUS_ERROR    = 'CHANGE';

    /**
     * @var string refund_id
     */
    private $id;

    /**
     * @var string out_refund_no
     */
    private $refno;

    /**
     * @var int refund_fee
     */
    private $total;

    /**
     * @var string refund_status
     */
    private $status;

    /**
     * @var \DateTimeImmutable|null refund_success_time
     */
    private $committedAt;

    /**
     * Constructor.
     *
     * @param string                  $id
     * @param string                  $refno
     * @param int                     $total
     * @param string                  $status
     * @param \DateTimeImmutable|null $committedAt
     */
    public function __construct(string $id, string $refno, int $total, string $status, ?\DateTimeImmutable $committedAt = null)
    {
        $this->id = $id;
        $this->refno = $refno;
        $this->total = $total;
        $this->status = $status;
        $this->committedAt = $committedAt;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return $this
     */
    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getRefno(): string
    {
        return $this->refno;
    }

    /**
     * @param string $refno
     *
     * @return $this
     */
    public function setRefno(string $refno): self
    {
        $this->refno = $refno;

        return $this;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     *
     * @return $this
     */
    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return $this
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getCommittedAt(): ?\DateTimeImmutable
    {
        return $this->committedAt;
    }

    /**
     * @param \DateTimeImmutable|null $committedAt
     *
     * @return $this
     */
    public function setCommittedAt(?\DateTimeImmutable $committedAt): self
    {
        $this->committedAt = $committedAt;

        return $this;
    }
}
