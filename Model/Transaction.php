<?php

/*
 * Transaction.php
 */

namespace AzureSpring\Wxpay\Model;

/**
 * Transaction
 */
class Transaction
{
    /**
     * transaction_id
     *
     * @var string
     */
    private $id;

    /**
     * out_trade_no
     *
     * @var string
     */
    private $tradeNo;

    /**
     * openid
     *
     * @var string
     */
    private $openId;

    /**
     * total_fee
     *
     * @var int
     */
    private $total;

    /**
     * time_end
     *
     * @var \DateTimeImmutable
     */
    private $committedAt;

    /**
     * attach
     *
     * @var string|null
     */
    private $attachment;

    /**
     * Constructor.
     *
     * @param string             $id
     * @param string             $tradeNo
     * @param string             $openId
     * @param int                $total
     * @param \DateTimeImmutable $committedAt
     * @param string|null        $attachment
     */
    public function __construct(string $id, string $tradeNo, string $openId, int $total, \DateTimeImmutable $committedAt, ?string $attachment = null)
    {
        $this->id          = $id;
        $this->tradeNo     = $tradeNo;
        $this->openId      = $openId;
        $this->total       = $total;
        $this->committedAt = $committedAt;
        $this->attachment  = $attachment;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTradeNo(): string
    {
        return $this->tradeNo;
    }

    /**
     * @return string
     */
    public function getOpenId(): string
    {
        return $this->openId;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCommittedAt(): \DateTimeImmutable
    {
        return $this->committedAt;
    }

    /**
     * @return null|string
     */
    public function getAttachment(): ?string
    {
        return $this->attachment;
    }
}
