<?php

/*
 * RefundOptions.php
 */

namespace AzureSpring\Wxpay\Model;

/**
 * RefundOptions
 */
class RefundOptions
{
    /**
     * out_refund_no - (merchant) refund id
     *
     * @var string
     */
    private $refundNo;

    /**
     * refund_fee - refund amount in cent
     *
     * @var int
     */
    private $total;

    /**
     * @var string TAG_TRANSACTION_ID | TAG_TRADE_NO
     */
    private $referenceTag;

    /**
     * transaction_id - (wechat) transaction id, or
     * out_trade_no - (merchant) order id
     *
     * @var string
     */
    private $referenceNo;

    /**
     * total_fee - payment amount in cent
     *
     * @var int
     */
    private $referenceTotal;

    /**
     * refund_fee_type - ISO 4217 currency code
     *
     * @var string|null
     */
    private $currencyCode;

    /**
     * refund_desc - refund reason (description)
     *
     * @var string|null
     */
    private $synopsis;

    /**
     * notify_url - async callback
     *
     * @var string|null
     */
    private $notifyUrl;

    /**
     * Constructor.
     *
     * @param string $refundNo
     * @param int    $total
     * @param string $referenceTag
     * @param string $referenceNo
     * @param int    $referenceTotal
     */
    public function __construct(string $refundNo, int $total, string $referenceTag, string $referenceNo, int $referenceTotal)
    {
        $this
            ->setRefundNo($refundNo)
            ->setTotal($total)
            ->setReferenceTag($referenceTag)
            ->setReferenceNo($referenceNo)
            ->setReferenceTotal($referenceTotal)
        ;
    }

    /**
     * @return string
     */
    public function getRefundNo(): string
    {
        return $this->refundNo;
    }

    /**
     * @param string $refundNo
     *
     * @return $this
     */
    public function setRefundNo(string $refundNo): self
    {
        $this->refundNo = $refundNo;

        return $this;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     *
     * @return $this
     */
    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return string
     */
    public function getReferenceTag(): string
    {
        return $this->referenceTag;
    }

    /**
     * @param string $referenceTag
     *
     * @return $this
     */
    public function setReferenceTag(string $referenceTag): self
    {
        if (!in_array($referenceTag, [Tag::TAG_TRANSACTION_ID, Tag::TAG_TRADE_NO])) {
            throw new \InvalidArgumentException();
        }
        $this->referenceTag = $referenceTag;

        return $this;
    }

    /**
     * @return string
     */
    public function getReferenceNo(): string
    {
        return $this->referenceNo;
    }

    /**
     * @param string $referenceNo
     *
     * @return $this
     */
    public function setReferenceNo(string $referenceNo): self
    {
        $this->referenceNo = $referenceNo;

        return $this;
    }

    /**
     * @return int
     */
    public function getReferenceTotal(): int
    {
        return $this->referenceTotal;
    }

    /**
     * @param int $referenceTotal
     *
     * @return $this
     */
    public function setReferenceTotal(int $referenceTotal): self
    {
        $this->referenceTotal = $referenceTotal;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }

    /**
     * @param string|null $currencyCode
     *
     * @return $this
     */
    public function setCurrencyCode(?string $currencyCode): self
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSynopsis(): ?string
    {
        return $this->synopsis;
    }

    /**
     * @param string|null $synopsis
     *
     * @return $this
     */
    public function setSynopsis(?string $synopsis): self
    {
        $this->synopsis = $synopsis;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNotifyUrl(): ?string
    {
        return $this->notifyUrl;
    }

    /**
     * @param string|null $notifyUrl
     *
     * @return $this
     */
    public function setNotifyUrl(?string $notifyUrl): self
    {
        $this->notifyUrl = $notifyUrl;

        return $this;
    }

    /**
     * @return string[]
     */
    public function dispose()
    {
        return [
            $this->referenceTag => $this->referenceNo,
            'out_refund_no'     => $this->refundNo,
            'total_fee'         => $this->referenceTotal,
            'refund_fee'        => $this->total,
            'refund_fee_type'   => $this->currencyCode,
            'refund_desc'       => $this->synopsis,
            'notify_url'        => $this->notifyUrl,
        ];
    }
}
