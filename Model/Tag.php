<?php

namespace AzureSpring\Wxpay\Model;

interface Tag
{
    const TAG_TRANSACTION_ID = 'transaction_id';
    const TAG_TRADE_NO       = 'out_trade_no';
    const TAG_REFUND_ID      = 'refund_id';
    const TAG_REFUND_NO      = 'out_refund_no';
}
