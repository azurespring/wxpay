<?php

/*
 * Payment.php
 */

namespace AzureSpring\Wxpay\Model;

/**
 * Payment
 */
class Payment
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string|null
     */
    private $url;

    /**
     * Constructor.
     *
     * @param string      $id
     * @param string|null $url
     */
    public function __construct(string $id, ?string $url = null)
    {
        $this->id = $id;
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return $this
     */
    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string|null $url
     *
     * @return $this
     */
    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }
}
