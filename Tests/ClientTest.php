<?php

/*
 * ClientTest.php
 */

namespace AzureSpring\Wxpay;

/**
 * ClientTest
 */
class ClientTest extends \PHPUnit\Framework\TestCase
{
    use \phpmock\phpunit\PHPMock;

    /**
     * @test
     */
    public function providePaymentOk()
    {
        $this
            ->getFunctionMock(__NAMESPACE__, 'time')
            ->expects($this->once())
            ->willReturn(1490840662)
        ;
        $nonce = $this->createMock(NonceInterface::class);
        $nonce
            ->method('take')
            ->willReturn('5K8264ILTKCH16CQ2502SI8ZNMTM67VS')
        ;
        $client = new Client(
            'wxd678efh567hg6787',
            '',
            'qazwsxedcrfvtgbyhnujmikolp111111',
            '',
            '',
            $nonce,
            $this->createMock(\GuzzleHttp\ClientInterface::class),
            $this->createMock(\Psr\Log\LoggerInterface::class)
        );

        $this->assertEquals(
            [
                'timeStamp' => '1490840662',
                'nonceStr'  => '5K8264ILTKCH16CQ2502SI8ZNMTM67VS',
                'package'   => 'prepay_id=wx2017033010242291fcfe0db70013231072',
                'signType'  => 'MD5',
                'paySign'   => '22D9B4E54AB1950F51E0649E8810ACD6',
            ],
            $client->provide(new Model\Payment('wx2017033010242291fcfe0db70013231072'))
        );
    }

    /**
     * @test
     */
    public function unenvelopOk()
    {
        $class = new \ReflectionClass(Client::class);
        $unenvelop = $class->getMethod('unenvelop');
        $unenvelop->setAccessible(true);

        $client = new Client(
            'wxd930ea5d5a258f4f',
            '10000100',
            '192006250b4c09247ec02edce69f6a2d',
            '',
            '',
            new OpensslNonce(),
            $this->createMock(\GuzzleHttp\ClientInterface::class),
            $this->createMock(\Psr\Log\LoggerInterface::class)
        );
        $params = $unenvelop->invoke($client, <<<__DOC__
<xml>
  <appid>wxd930ea5d5a258f4f</appid>
  <mch_id>10000100</mch_id>
  <device_info>1000</device_info>
  <body>test</body>
  <nonce_str>ibuaiVcKdpRxkhJA</nonce_str>
  <sign>9A0A8659F005D6984697E2CA0A9CF3B7</sign>
</xml>
__DOC__
        );

        $this->assertEquals([
            'appid'       => 'wxd930ea5d5a258f4f',
            'mch_id'      => '10000100',
            'device_info' => '1000',
            'body'        => 'test',
            'nonce_str'   => 'ibuaiVcKdpRxkhJA',
            'sign'        => '9A0A8659F005D6984697E2CA0A9CF3B7',
        ], $params);
    }

    /**
     * @test
     */
    public function checkOk()
    {
        $class = new \ReflectionClass(Client::class);
        $check = $class->getMethod('check');
        $check->setAccessible(true);

        $client = new Client(
            'wxd930ea5d5a258f4f',
            '10000100',
            '192006250b4c09247ec02edce69f6a2d',
            '',
            '',
            new OpensslNonce(),
            $this->createMock(\GuzzleHttp\ClientInterface::class),
            $this->createMock(\Psr\Log\LoggerInterface::class)
        );
        $this->assertTrue($check->invoke($client, [
            'appid'       => 'wxd930ea5d5a258f4f',
            'mch_id'      => '10000100',
            'device_info' => '1000',
            'body'        => 'test',
            'nonce_str'   => 'ibuaiVcKdpRxkhJA',
            'sign'        => '9A0A8659F005D6984697E2CA0A9CF3B7',
        ]));
    }

    /**
     * @test
     */
    public function envelopOk()
    {
        $class = new \ReflectionClass(Client::class);
        $envelop = $class->getMethod('envelop');
        $envelop->setAccessible(true);

        $client = new Client(
            'wxd930ea5d5a258f4f',
            '10000100',
            '192006250b4c09247ec02edce69f6a2d',
            '',
            '',
            new OpensslNonce(),
            $this->createMock(\GuzzleHttp\ClientInterface::class),
            $this->createMock(\Psr\Log\LoggerInterface::class)
        );
        $body = $envelop->invoke($client, [
            'appid'       => 'wxd930ea5d5a258f4f',
            'mch_id'      => '10000100',
            'device_info' => '1000',
            'body'        => 'test',
            'nonce_str'   => 'ibuaiVcKdpRxkhJA',
        ]);

        $this->assertXmlStringEqualsXmlString(
            <<<__XML__
<xml>
  <appid>wxd930ea5d5a258f4f</appid>
  <mch_id>10000100</mch_id>
  <device_info>1000</device_info>
  <body>test</body>
  <nonce_str>ibuaiVcKdpRxkhJA</nonce_str>
  <sign>9A0A8659F005D6984697E2CA0A9CF3B7</sign>
</xml>
__XML__
            ,
            $body
        );
    }

    /**
     * @test
     *
     * @throws \ReflectionException
     */
    public function signOk()
    {
        $class = new \ReflectionClass(Client::class);
        $sign = $class->getMethod('sign');
        $sign->setAccessible(true);

        $client = new Client(
            'wxd930ea5d5a258f4f',
            '10000100',
            '192006250b4c09247ec02edce69f6a2d',
            '',
            '',
            new OpensslNonce(),
            $this->createMock(\GuzzleHttp\ClientInterface::class),
            $this->createMock(\Psr\Log\LoggerInterface::class)
        );
        $signature = $sign->invoke($client, [
            'appid'       => 'wxd930ea5d5a258f4f',
            'mch_id'      => '10000100',
            'device_info' => '1000',
            'body'        => 'test',
            'nonce_str'   => 'ibuaiVcKdpRxkhJA',
        ]);

        $this->assertEquals('9A0A8659F005D6984697E2CA0A9CF3B7', $signature);
    }

    /**
     * @test
     *
     * @throws \ReflectionException
     */
    public function signZeroValuesOk()
    {
        $class = new \ReflectionClass(Client::class);
        $sign = $class->getMethod('sign');
        $sign->setAccessible(true);

        $client = new Client(
            'wxd930ea5d5a258f4f',
            '10000100',
            '192006250b4c09247ec02edce69f6a2d',
            '',
            '',
            new OpensslNonce(),
            $this->createMock(\GuzzleHttp\ClientInterface::class),
            $this->createMock(\Psr\Log\LoggerInterface::class)
        );
        $signature = $sign->invoke($client, [
            'return_code'         => 'SUCCESS',
            'return_msg'          => 'OK',
            'appid'               => 'wxd930ea5d5a258f4f',
            'mch_id'              => '10000100',
            'nonce_str'           => 'EJQ6BhqaXF5YxgLZ',
            'result_code'         => 'SUCCESS',
            'transaction_id'      => '4200000163201810089127103556',
            'out_trade_no'        => 'wdKU64nYT2yRfrEmOdZatw',
            'out_refund_no'       => 'i0OgeXJ6QgSXqScvtT1YEA',
            'refund_id'           => '50000408512018100906678625234',
            'refund_channel'      => '',
            'refund_fee'          => '1',
            'coupon_refund_fee'   => '0',
            'total_fee'           => '1',
            'cash_fee'            => '1',
            'coupon_refund_count' => '0',
            'cash_refund_fee'     => '1',
        ]);

        $this->assertEquals('64844F2F203D47B7EF4486B84193A2A8', $signature);
    }
}
