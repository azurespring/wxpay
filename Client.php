<?php

/*
 * Client.php
 */

namespace AzureSpring\Wxpay;

use GuzzleHttp\ClientInterface as Guzzle;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

/**
 * Client
 */
class Client
{
    const BASE_URI = 'https://api.mch.weixin.qq.com';

    private $appId;

    private $tradeType;

    private $merchantId;

    private $secret;

    private $privKey;

    private $certificate;

    private $nonce;

    private $guzzle;

    private $responseFactory;

    private $logger;

    /**
     * Constructor.
     *
     * @param string                   $appId
     * @param string                   $tradeType
     * @param string                   $merchantId
     * @param string                   $secret          api key
     * @param string|null              $certificate     (ssl) certificate
     * @param string|null              $privKey         (ssl) private key
     * @param NonceInterface           $nonce
     * @param Guzzle                   $guzzle
     * @param ResponseFactoryInterface $responseFactory
     * @param LoggerInterface          $logger
     */
    public function __construct(string $appId, string $tradeType, string $merchantId, string $secret, ?string $certificate, ?string $privKey, NonceInterface $nonce, Guzzle $guzzle, ResponseFactoryInterface $responseFactory, LoggerInterface $logger)
    {
        $this->appId = $appId;
        $this->tradeType = $tradeType;
        $this->merchantId = $merchantId;
        $this->secret = $secret;
        $this->certificate = $certificate;
        $this->privKey = $privKey;
        $this->nonce = $nonce;
        $this->guzzle = $guzzle;
        $this->responseFactory = $responseFactory;
        $this->logger = $logger;
    }

    /**
     * https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=9_1
     * https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=9_1
     *
     * @param Model\PaymentOptions $opts
     *
     * @return Model\Payment
     *
     * @throws Exception\WxpayException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createPayment(Model\PaymentOptions $opts): Model\Payment
    {
        if ($opts->getTradeType()) {
            $opts->setTradeType($this->tradeType);
        }

        $res = $this->request('/pay/unifiedorder', $opts->dispose());
        $this->assertEquals($this->appId, $res['appid'], 'App id does not match');
        $this->assertEquals($this->merchantId, $res['mch_id'], 'Merchant id does not match');
        $this->assertEquals($opts->getTradeType(), $res['trade_type'], 'Trade type does not match');

        return new Model\Payment($res['prepay_id'], @$res['code_url']);
    }

    /**
     * https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=9_2&index=4
     * https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=9_2
     *
     * @param string $tag
     * @param string $ref
     *
     * @return array
     *
     * @throws Exception\WxpayException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function findPayment(string $tag, string $ref)
    {
        assert(in_array($tag, [Model\Tag::TAG_TRADE_NO, Model\Tag::TAG_TRANSACTION_ID]));
        $res = $this->request('/pay/orderquery', [
            $tag => $ref,
        ]);

        return [$res['trade_state'], $res['trade_state_desc'], $res];
    }

    /**
     * https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=9_12&index=2
     * https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=7_7&index=5
     *
     * @param Model\Payment $payment
     * @param string|null   $type
     *
     * @return mixed
     */
    public function provide(Model\Payment $payment, ?string $type = null)
    {
        switch ($type ?? $this->tradeType) {
            case Model\PaymentOptions::TYPE_APP:
                $params = [
                    'appid' => $this->appId,
                    'partnerid' => $this->merchantId,
                    'prepayid' => $payment->getId(),
                    'package' => 'Sign=WXPay',
                    'noncestr' => $this->nonce->take(16),
                    'timestamp' => (string) time(),
                ];

                $params['sign'] = $this->sign($params);
                unset($params['appid']);

                return $params;

            default:
                $params = [
                    'appId'     => $this->appId,
                    'timeStamp' => (string) time(),
                    'nonceStr'  => $this->nonce->take(16),
                    'package'   => "prepay_id={$payment->getId()}",
                    'signType'  => 'MD5',
                ];

                $params['paySign'] = $this->sign($params);
                unset($params['appId']);

                return $params;
        }
    }

    /**
     * https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=9_7&index=3
     * https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=9_7
     *
     * @param RequestInterface $request
     *
     * @return Model\Transaction
     */
    public function accept(RequestInterface $request): Model\Transaction
    {
        $body = $this->sanitize($request->getBody()->getContents());

        return new Model\Transaction(
            $body['transaction_id'],
            $body['out_trade_no'],
            $body['openid'],
            $body['total_fee'],
            \DateTimeImmutable::createFromFormat('YmdHis', $body['time_end'], new \DateTimeZone('Asia/Shanghai')),
            @$body['attach']
        );
    }

    /**
     * https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=9_7&index=3
     * https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=9_7
     *
     * @param bool        $ok
     * @param string|null $message
     *
     * @return ResponseInterface
     */
    public function answer(bool $ok = true, ?string $message = null): ResponseInterface
    {
        $res = $this->responseFactory->createResponse();
        $res->getBody()->write($this->archive(array_filter([
            'return_code' => $ok ? 'SUCCESS' : 'FAIL',
            'return_msg'  => $message,
        ])));

        return $res->withHeader('Content-type', 'text/xml; charset="utf-8"');
    }

    /**
     * https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=9_4&index=6
     * https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=9_4
     *
     * @param Model\RefundOptions $opts
     *
     * @return string
     *
     * @throws Exception\WxpayException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function refund(Model\RefundOptions $opts): string
    {
        $res = $this->request('/secapi/pay/refund', $opts->dispose(), true);
        $this->assertEquals($opts->getRefundNo(), $res['out_refund_no'], 'Refund no does not match');
        $this->assertEquals($opts->getTotal(), $res['refund_fee'], 'Total does not match');
        $this->assertEquals($opts->getReferenceNo(), $res[$opts->getReferenceTag()], "Reference no ({$opts->getReferenceTag()}) does not match");
        $this->assertEquals($opts->getReferenceTotal(), $res['total_fee'], 'Reference total does not match');

        return $res['refund_id'];
    }

    /**
     * https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=9_5&index=7
     * https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=9_5
     *
     * @param string   $tag    TAG_TRANSACTION_ID|TAG_TRADE_NO|TAG_REFUND_ID|TAG_REFUND_NO
     * @param string   $ref
     * @param int|null $offset
     *
     * @return Model\RefundFragment
     *
     * @throws Exception\WxpayException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function findRefunds(string $tag, string $ref, ?int $offset = null): Model\RefundFragment
    {
        $res = $this->request('/pay/refundquery', [
            $tag     => $ref,
            'offset' => $offset,
        ]);
        $fragment = new Model\RefundFragment(
            $res['transaction_id'],
            $res['out_trade_no'],
            $res['total_fee'],
            @$res['total_refund_count'] ?? $res['refund_count'],
            array_map(
                function ($i) use ($res) {
                    if ($timestamp = @$res["refund_success_time_{$i}"]) {
                        $timestamp = \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $timestamp, new \DateTimeZone('Asia/Shanghai'));
                    }

                    return new Model\Refund(
                        $res["refund_id_{$i}"],
                        $res["out_refund_no_{$i}"],
                        $res["refund_fee_{$i}"],
                        $res["refund_status_{$i}"],
                        $timestamp
                    );
                },
                range(0, $res['refund_count'] - 1)
            )
        );

        return $fragment->setCurrencyCode(@$res['fee_type']);
    }

    /**
     * @param string $uri
     * @param array  $params
     * @param bool   $secure ssl
     *
     * @return array
     *
     * @throws Exception\WxpayException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function request(string $uri, array $params, bool $secure = false)
    {
        $params = array_merge($params, [
            'appid'     => $this->appId,
            'mch_id'    => $this->merchantId,
            'nonce_str' => $this->nonce->take(16),
        ]);
        $res = $this
            ->guzzle
            ->request('POST', $uri, array_merge(
                [
                    'headers' => ['Content-type' => 'text/xml; charset="utf-8"'],
                    'body'    => $this->envelop($params),
                ],
                $secure ? [
                    'cert'    => $this->certificate,
                    'ssl_key' => $this->privKey,
                ] : [
                ]
            ))
            ->getBody()
            ->getContents()
        ;
        $this->logger->debug("request {$uri}", [
            'params' => $params,
            'response' => $res,
        ]);

        return $this->sanitize($res);
    }

    /**
     * @param string $body
     *
     * @return array|null
     */
    protected function sanitize(string $body): ?array
    {
        $document = $this->unenvelop($body);
        if ('SUCCESS' !== $document['return_code']) {
            throw new Exception\TransferException($document['return_msg']);
        }
        if ('SUCCESS' !== $document['result_code']) {
            throw new Exception\ServiceException($document['err_code_des'], $document['err_code']);
        }
        if (!$this->check($document)) {
            throw new Exception\SignatureException();
        }
        $this->assertEquals($this->appId, $document['appid'], 'App id does not match');
        $this->assertEquals($this->merchantId, $document['mch_id'], 'Merchant id does not match');

        return $document;
    }

    /**
     * @param string $data
     *
     * @return array|null
     */
    protected function unenvelop(string $data): ?array
    {
        $this->logger->debug('unenvelop()', ['data' => $data]);
        $document = new \DOMDocument();
        $document->loadXML($data, LIBXML_NOBLANKS);

        return array_reduce(
            iterator_to_array($document->firstChild->childNodes),
            function ($params, \DOMElement $el) {
                $params[$el->tagName] = $el->textContent;

                return $params;
            },
            []
        );
    }

    /**
     * @param string[] $params
     *
     * @return bool
     */
    protected function check(array $params): bool
    {
        $signature = $this->sign(array_filter(
            $params,
            function ($k) {
                return 'sign' !== $k;
            },
            ARRAY_FILTER_USE_KEY
        ));

        return $signature === $params['sign'];
    }

    /**
     * @param string[] $params
     *
     * @return string
     */
    protected function envelop(array $params): string
    {
        $params['sign'] = $this->sign($params);

        return $this->archive($params);
    }

    /**
     * @param string[] $params
     *
     * @return string
     */
    protected function sign(array $params): string
    {
        $params = array_filter($params, function ($v) {
            return $v !== null && $v !== '';
        });
        $params = array_map(
            function ($k, $v) {
                return "{$k}={$v}";
            },
            array_keys($params),
            $params
        );
        sort($params);
        $params[] = "key={$this->secret}";

        return strtoupper(md5(implode('&', $params)));
    }

    /**
     * @param mixed  $expected
     * @param mixed  $actual
     * @param string $message
     */
    protected function assertEquals($expected, $actual, string $message)
    {
        if ((string) $expected !== $actual) {
            $this->logger->error($message, [
                'expected' => $expected,
                'actual'   => $actual,
            ]);

            throw new \UnexpectedValueException($message);
        }
    }

    /**
     * @param array $params
     *
     * @return string
     */
    protected function archive(array $params): string
    {
        $document = new \DOMDocument();
        $root = $document->appendChild($document->createElement('xml'));
        foreach ($params as $k => $v) {
            $el = $root->appendChild($document->createElement($k));
            $el->appendChild($document->createCDATASection($v));
        }

        return $document->saveXML($root);
    }
}
