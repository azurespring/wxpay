<?php

require __DIR__.'/../vendor/autoload.php';

use AzureSpring\Wxpay;
use Slim\Http\Request;
use Slim\Http\Response;

$app = new Slim\App(new Slim\Container());
$app->add(new RKA\Middleware\IpAddress());
$app->getContainer()['logger'] = function () {
    return new Monolog\Logger('app');
};
$app->getContainer()['wxpay'] = function ($c) {
    return new Wxpay\Client(
        getenv('WXPAY_APP_ID'),
        getenv('WXPAY_MERCHANT_ID'),
        getenv('WXPAY_SECRET'),
        getenv('WXPAY_CERTIFICATE'),
        getenv('WXPAY_PRIV_KEY'),
        new Wxpay\OpensslNonce(),
        new GuzzleHttp\Client([
            'base_uri' => Wxpay\Client::BASE_URI,
        ]),
        $c['logger']
    );
};

$app->post('/payments', function (Request $req, Response $res) {
    $body = $req->getParsedBody();
    $payment = $this->wxpay->createPayment(
        (new Wxpay\Model\PaymentOptions(
            $body['trade_no'],
            $body['synopsis'],
            $body['total'],
            @$body['ip'] ?: $req->getAttribute('ip_address'),
            $body['notify_url']
        ))
        ->setOpenId($body['openid'])
    );

    return $res->withJson($this->wxpay->provide($payment));
});

$app->post('/envelopes', function (Request $req, Response $res) {
    try {
        /**
         * @var Wxpay\Model\Transaction $tx
         */
        $tx = $this->wxpay->accept($req);
        $this->logger->info('Gotcha', [
            'id'          => $tx->getId(),
            'tradeNo'     => $tx->getTradeNo(),
            'openId'      => $tx->getOpenId(),
            'total'       => $tx->getTotal(),
            'committedAt' => $tx->getCommittedAt(),
        ]);

        return $this->wxpay->answer($res, true);
    } catch (\Exception $e) {
        return $this->wxpay->answer($res, false, $e->getMessage());
    }
});

$app->post('/refunds', function (Request $req, Response $res) {
    $body = $req->getParsedBody();
    $id = $this->wxpay->refund(
        (new Wxpay\Model\RefundOptions(
            $body['refund_no'],
            $body['total'],
            $body['reference_tag'],
            $body['reference_no'],
            $body['reference_total']
        ))
        ->setSynopsis(@$body['synopsis'])
        ->setNotifyUrl(@$body['notify_url'])
    );

    return $res->withJson(['id' => $id]);
});

$app->get('/refunds', function (Request $req, Response $res) {
    $fragment = $this->wxpay->findRefunds($req->getQueryParam('tag'), $req->getQueryParam('ref'), $req->getQueryParam('offset'));

    return $res->withJson([
        'id'    => $fragment->getId(),
        'refno' => $fragment->getRefno(),
        'total' => $fragment->getTotal(),
        'count' => $fragment->getCount(),
        'elements' => array_map(
            function ($refund) {
                if ($committedAt = $refund->getCommittedAt()) {
                    $committedAt = $committedAt->format(\DateTime::ATOM);
                }

                return [
                    'id'           => $refund->getId(),
                    'refno'        => $refund->getRefno(),
                    'total'        => $refund->getTotal(),
                    'status'       => $refund->getStatus(),
                    'committed_at' => $committedAt,
                ];
            },
            $fragment->getElements()
        ),
    ]);
});

$app->run();
