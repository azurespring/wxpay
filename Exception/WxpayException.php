<?php

/*
 * WxpayException.php
 */
namespace AzureSpring\Wxpay\Exception;

/**
 * @method string getMessage()
 * @method \Throwable|null getPrevious()
 * @method mixed|int getCode()
 * @method string getFile()
 * @method int getLine()
 * @method array getTrace()
 * @method string getTraceAsString()
 */
interface WxpayException {}
